#!/bin/bash


txt2man -d "${CHANGELOG_DATE}" -t OPENEMS     -s 1 openEMS.txt          > openEMS.1
txt2man -d "${CHANGELOG_DATE}" -t NF2FF       -s 1 nf2ff.txt            > nf2ff.1
txt2man -d "${CHANGELOG_DATE}" -t APPCSXCAD   -s 1 AppCSXCAD.txt        > AppCSXCAD.1
