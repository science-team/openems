openems (0.0.36+dfsg-1) UNRELEASED; urgency=medium

  * Team upload.
  * New upstream version
  * Point watch file to Github
  * Standards-Version: 4.6.2 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 21 Jan 2024 13:24:38 +0100

openems (0.0.35+git20190103.6a75e98+dfsg.1-3) unstable; urgency=medium

  * Team upload.
  * Add 0008-Fix-SmoothMeshLines.py-TypeError.patch.  Thanks g.l. gragnani for
    the report and the pointer!
    Closes: #982471
  * debian/tests/control:
    - mark tests superficial, as it failed to catch the above issue;
    - allow-stderr due to matplotlib cache message not critical yet unfortunate.

 -- Étienne Mollier <etienne.mollier@mailoo.org>  Sun, 21 Feb 2021 12:09:58 +0100

openems (0.0.35+git20190103.6a75e98+dfsg.1-2) unstable; urgency=medium

  * Team upload.
  * Build-Depends: python3-all-dev
    not python3-all, to provide Python.h. Closes: #944144, #946264.

 -- Drew Parsons <dparsons@debian.org>  Mon, 09 Dec 2019 15:54:42 +0800

openems (0.0.35+git20190103.6a75e98+dfsg.1-1) unstable; urgency=medium

  * Upload to unstable

  [ Gilles Filippini ]
  * Fix FTBFS against HDF5 1.10.5 (Closes: #931539)

  [ Ruben Undheim ]
  * debian/control:
    - New standards version 4.4.1 - no changes
    - DH level 12
    - Use debhelper-compat
  * Drop Python 2 package

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 19 Oct 2019 19:20:44 +0200

openems (0.0.35+dfsg.1-4) unstable; urgency=medium

  * Team upload.
  * Fix building against Octave 5

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 07 Oct 2019 14:02:23 +0200

openems (0.0.35+git20190103.6a75e98+dfsg.1-1~exp2) experimental; urgency=medium

  * d/patches/0007-Fix-some-imports-to-make-python-interface-work.patch:
    - Use 'from __future__ import absolute_import' instead to fix import in
      Python2 package
  * debian/control:
    - Add missing install dependencies python3-matplotlib and
      python-matplotlib.
  * debian/gitlab-ci.yml:
    - Enable continuous integration on salsa

 -- Ruben Undheim <ruben.undheim@gmail.com>  Thu, 21 Feb 2019 23:19:00 +0100

openems (0.0.35+git20190103.6a75e98+dfsg.1-1~exp1) experimental; urgency=low

  * New upstream GIT HEAD with Python interface support
    - Refreshed patches
  * debian/control:
    - New standards version 4.3.0 - no changes
    - New binary package: python-openems
    - New binary package: python3-openems
    - Depend on libvtk7-qt-dev instead of libvtk6-qt-dev
    - More build-dependencies for Python interface:
      - cython, cython3, dh-python, python-all, python3-all
  * debian/libopenems.install:
    - Install more header files
  * Added debian/python-openems.install
  * Added debian/python3-openems.install
  * d/patches/0007-Fix-some-imports-to-make-python-interface-work.patch:
    - Fix import for Python 2

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 10 Feb 2019 09:14:26 +0000

openems (0.0.35+dfsg.1-3) unstable; urgency=medium

  * debian/changelog:
    - Fix typo
  * debian/control:
    - Do not build-depend on g++-7 anymore
  * debian/rules:
    - Now build with default g++ version
  * debian/tests/octave-load:
    - Handle case when run in a non-X11 environment

 -- Ruben Undheim <ruben.undheim@gmail.com>  Tue, 27 Nov 2018 18:59:05 +0100

openems (0.0.35+dfsg.1-2) unstable; urgency=medium

  * d/patches/0007-Guard-xmmintrin.h-include-so-it-is-only-used-when-ne.patch
    - Cherry-pick fix from upstream: a5a1dca8324aa12dfbf8e268fbc06c2310b91c69
  * debian/rules: Set -DSSE_CORRECT_DENORMALS=0

 -- Ruben Undheim <ruben.undheim@gmail.com>  Fri, 23 Nov 2018 21:22:01 +0100

openems (0.0.35+dfsg.1-1) unstable; urgency=low

  * Initial release (Closes: #830109)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 02 Sep 2018 01:07:06 +0200
